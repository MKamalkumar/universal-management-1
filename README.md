ReadMe[](https://troweprice.com/v1/entity)

**Universal Entity Management API
Overview**

The Universal Entity Management API accepts HTTPS/TLS connections only in order to protect the integrity and confidentiality of any data transferred. HTTP connections are refused - plaintext communication is not supported.

As a further means to secure the API, all API requests are authenticated through OAuth 2.0 Client Credentials.

image

Business Functions
Universal Entity Management microservices performs Consumer/Client (Entity) Add, update and search Consumer/Client by Name or Entity ID

API Details
URL: https://troweprice.com/v1/entity

HTTP Methods

Search Consumer/Client by Name or Entity ID (GET)

Parameters
Query: entityName (String) - Entity Name to search (Optional)
Query: entityID (String) - Entity ID to search (Optional)

Response Type - Object
Request Body - "Entity ID" - String 32 Bytes
"Entity Name" - String 40 Bytes Max
"Entity Address Line 1" - String 40 Bytes Max
"Entity Address Line 2" - String 40 Bytes Max
"Entity Address Line 3" - String 40 Bytes Max
"Entity Address ZIPCode " - String 10 Bytes Max
"Entity Phone No" - Numeric 10 Bytes
"Entity Tax Id" - Numeric 10 Bytes Max

Add Consumer/Client (Entity) POST

Request Body - (Object)

"Entity Name" - String 40 Bytes Max (Required)
"Entity Address Line 1" - String 40 Bytes Max
"Entity Address Line 2" - String 40 Bytes Max
"Entity Address Line 3" - String 40 Bytes Max
"Entity Address ZIPCode " - String 10 Bytes Max
"Entity Phone No" - Numeric 10 Bytes
"Entity Tax Id" - Numeric 10 Bytes Max (Required)

Response Data
"Entity ID" - String 32 Bytes

Modify Consumer/Client (Entity) PUT

Request Body - (Object)

"Entity ID" - String 32 Bytes (Required)
"Entity Name" - String 40 Bytes Max
"Entity Address Line 1" - String 40 Bytes Max
"Entity Address Line 2" - String 40 Bytes Max
"Entity Address Line 3" - String 40 Bytes Max
"Entity Address ZIPCode " - String 10 Bytes Max
"Entity Phone No" - Numeric 10 Bytes
"Entity Tax Id" - Numeric 10 Bytes Max

Response Data
"Entity ID" - String 32 Bytes

Response codes

200 OK: Success!
201 OK: Created!
400 Bad Request: The request cannot be fulfilled due to bad syntax or an invalid parameter.
404 Not Found: Resource was not found.
405 Method Not Found: A request was made of a resource using a request method not supported by that resource.
500 Internal Server Error: Something is broken on the system’s side.
